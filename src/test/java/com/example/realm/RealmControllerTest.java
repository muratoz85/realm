package com.example.realm;

import com.example.realm.util.API;
import com.example.realm.entity.Realm;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith( SpringRunner.class )
@SpringBootTest( webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT )
@AutoConfigureMockMvc
@TestPropertySource( "classpath:application.properties" )
public class RealmControllerTest
{
	@Autowired
	private MockMvc mockMvc;

	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	private static Realm createRealm( String name )
	{
		return createRealm( name, null );
	}

	private static Realm createRealm( String name, String description )
	{
		// creating a builder pattern seems overkill for this case

		Realm realm = new Realm();
		realm.setName( name );
		realm.setDescription( description );
		return realm;
	}

	private ResultActions performPostRequest( Realm realm ) throws Exception
	{
		return mockMvc.perform( post( API.REALM ).contentType( MediaType.APPLICATION_JSON )
										.characterEncoding( "UTF-8" )
										.content( OBJECT_MAPPER.writeValueAsString( realm ) ) );
	}

	private ResultActions performGetRequest( String id ) throws Exception
	{
		return mockMvc.perform( get( API.REALM + "/" + id ) );
	}

	@Test
	public void testGet_InvalidArgument() throws Exception
	{
		performGetRequest( "3.5" ) //
				.andExpect( status().is4xxClientError() );
	}

	@Test
	public void testCreate() throws Exception
	{
		String name = "realm1";
		String description = "description1";

		performPostRequest( createRealm( name, description ) ).andExpect( status().isOk() );

		performGetRequest( "1" ) //
				.andExpect( jsonPath( "$.name" ).value( name ) )
				.andExpect( jsonPath( "$.description" ).value( description ) );
	}

	@Test
	public void testCreate_InvalidRealmName() throws Exception
	{
		performPostRequest( createRealm( null, "randomDescription" ) ) //
				.andExpect( status().is4xxClientError() );
	}

	@Test
	public void testCreate_DuplicateRealmName() throws Exception
	{
		String name = "realmDuplicate";

		performPostRequest( createRealm( name ) ).andExpect( status().isOk() );
		performPostRequest( createRealm( name ) ).andExpect( status().is4xxClientError() );
	}
}
