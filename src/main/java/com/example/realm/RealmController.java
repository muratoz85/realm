package com.example.realm;

import com.example.realm.request.RealmCreateRequest;
import com.example.realm.entity.Realm;
import com.example.realm.service.RealmService;
import com.example.realm.validation.exception.AbstractCustomException;
import com.example.realm.validation.exception.DuplicateRealmException;
import com.example.realm.validation.exception.InvalidArgumentException;
import com.example.realm.validation.exception.InvalidRealmNameException;
import com.example.realm.validation.exception.RealmNotFoundException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

import static com.example.realm.util.API.REALM;

@RestController
@RequestMapping( REALM )
public class RealmController
{
	@Autowired
	private RealmService realmService;

	public static final String PATH_ID = "/{id}";

	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	@RequestMapping( value = PATH_ID, method = RequestMethod.GET )
	public Realm get( @PathVariable String id ) throws RealmNotFoundException, InvalidArgumentException
	{
		return realmService.getById( id );
	}

	@RequestMapping( method = RequestMethod.POST )
	public void create( @RequestBody RealmCreateRequest request ) throws InvalidRealmNameException, DuplicateRealmException
	{
		realmService.create( OBJECT_MAPPER.convertValue( request, Realm.class ) );
	}

	@ExceptionHandler
	void handleException( AbstractCustomException e, HttpServletResponse response ) throws Exception
	{
		response.sendError( e.getResponseCode(), e.getErrorCode() );
	}

	@ExceptionHandler
	void handleException( HttpMessageNotReadableException e, HttpServletResponse response ) throws Exception
	{
		response.sendError( HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase() );
	}

	@ExceptionHandler
	void handleException( Exception e, HttpServletResponse response ) throws Exception
	{
		response.sendError( HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase() );
	}
}
