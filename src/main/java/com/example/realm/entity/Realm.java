package com.example.realm.entity;

import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table( name = Realm.TABLE_NAME )
public class Realm
{
	// DDL fields for DB config case sensitivity issues (for migration in the future)
	public static final String TABLE_NAME = "REALM";
	private static final String PROP_ID = "ID";
	private static final String PROP_NAME = "NAME";
	private static final String PROP_DESCRIPTION = "DESCRIPTION";
	private static final String PROP_KEY = "ENCRYPTION_KEY";
	public static final int KEY_LENGTH = 32;

	@Id
	@GeneratedValue
	@Column( name = PROP_ID )
	private Long id;

	@Column( name = PROP_NAME, unique = true, nullable = false )
	private String name;

	@Column( name = PROP_DESCRIPTION )
	private String description;

	@Column( name = PROP_KEY, nullable = false )
	@Length( min = KEY_LENGTH, max = KEY_LENGTH )
	private String key;

	public Long getId()
	{
		return id;
	}

	public void setId( Long id )
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName( String name )
	{
		this.name = name;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription( String description )
	{
		this.description = description;
	}

	public String getKey()
	{
		return key;
	}

	public void setKey( String key )
	{
		this.key = key;
	}
}
