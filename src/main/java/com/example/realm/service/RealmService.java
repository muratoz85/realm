package com.example.realm.service;

import com.example.realm.entity.Realm;
import com.example.realm.repository.RealmRepository;
import com.example.realm.util.RealmKeyGenerator;
import com.example.realm.validation.exception.DuplicateRealmException;
import com.example.realm.validation.exception.InvalidArgumentException;
import com.example.realm.validation.exception.InvalidRealmNameException;
import com.example.realm.validation.exception.RealmNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class RealmService
{
	@Autowired
	private RealmRepository realmRepository;

	public Realm getById( @NotNull final String idText ) throws RealmNotFoundException, InvalidArgumentException
	{
		Optional<Realm> realmOptional = realmRepository.findById( getValidatedId( idText ) );
		return getValidatedGetResult( realmOptional );
	}

	public void create( @NotNull final Realm realm ) throws InvalidRealmNameException, DuplicateRealmException
	{
		validateCreate( realm );
		realm.setKey( RealmKeyGenerator.generateKey() );
		realmRepository.save( realm );
	}

	private Long getValidatedId( String idText ) throws InvalidArgumentException
	{
		try
		{
			return Long.parseLong( idText );
		}
		catch ( NumberFormatException e )
		{
			throw new InvalidArgumentException();
		}
	}

	private Realm getValidatedGetResult( Optional<Realm> optionalRealm ) throws RealmNotFoundException
	{
		try
		{
			return optionalRealm.get();
		}
		catch ( NoSuchElementException e )
		{
			throw new RealmNotFoundException();
		}
	}

	private void validateCreate( Realm realm ) throws InvalidRealmNameException, DuplicateRealmException
	{
		checkNonNullName( realm.getName() );
		checkDuplicateName( realm.getName() );
	}

	private void checkNonNullName( String name ) throws InvalidRealmNameException
	{
		if ( name == null )
		{
			throw new InvalidRealmNameException();
		}
	}

	private void checkDuplicateName( String name ) throws DuplicateRealmException
	{
		if ( realmRepository.findByName( name ) != null )
		{
			throw new DuplicateRealmException();
		}
	}
}
