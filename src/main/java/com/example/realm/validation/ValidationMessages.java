package com.example.realm.validation;

public final class ValidationMessages
{
	private ValidationMessages()
	{
	}

	public static final String INVALID_ARGUMENT = "InvalidArgument";
	public static final String REALM_NOT_FOUND = "RealmNotFound";
	public static final String INVALID_REALM_NAME = "InvalidRealmName";
	public static final String DUPLICATE_REALM_NAME = "DuplicateRealmName";
}
