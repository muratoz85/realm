package com.example.realm.validation.exception;

import com.example.realm.validation.ValidationMessages;
import org.springframework.http.HttpStatus;

public class RealmNotFoundException extends AbstractCustomException
{
	public RealmNotFoundException()
	{
		super( HttpStatus.NOT_FOUND, ValidationMessages.REALM_NOT_FOUND );
	}
}
