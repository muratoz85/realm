package com.example.realm.validation.exception;

import com.example.realm.validation.ValidationMessages;
import org.springframework.http.HttpStatus;

public class DuplicateRealmException extends AbstractCustomException
{
	public DuplicateRealmException()
	{
		super( HttpStatus.CONFLICT, ValidationMessages.DUPLICATE_REALM_NAME );
	}
}
