package com.example.realm.validation.exception;

import org.springframework.http.HttpStatus;

public abstract class AbstractCustomException extends Exception
{
	protected final int responseCode;
	protected final String errorCode;

	public AbstractCustomException( HttpStatus status, String errorCode )
	{
		super( status.getReasonPhrase() );
		this.responseCode = status.value();
		this.errorCode = errorCode;
	}

	public int getResponseCode()
	{
		return responseCode;
	}

	public String getErrorCode()
	{
		return errorCode;
	}
}
