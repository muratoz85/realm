package com.example.realm.validation.exception;

import com.example.realm.validation.ValidationMessages;
import org.springframework.http.HttpStatus;

public class InvalidArgumentException extends AbstractCustomException
{
	public InvalidArgumentException()
	{
		super( HttpStatus.BAD_REQUEST, ValidationMessages.INVALID_ARGUMENT );
	}
}
