package com.example.realm.validation.exception;

import com.example.realm.validation.ValidationMessages;
import org.springframework.http.HttpStatus;

public class InvalidRealmNameException extends AbstractCustomException
{
	public InvalidRealmNameException()
	{
		super( HttpStatus.BAD_REQUEST, ValidationMessages.INVALID_REALM_NAME );
	}
}
