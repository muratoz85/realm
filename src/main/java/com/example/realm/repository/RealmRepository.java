package com.example.realm.repository;

import com.example.realm.entity.Realm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RealmRepository extends JpaRepository<Realm, Long>
{
	Realm findByName( String name );
}
