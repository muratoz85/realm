package com.example.realm.util;

import com.example.realm.entity.Realm;

import java.util.Random;

public final class RealmKeyGenerator
{
	private RealmKeyGenerator()
	{
	}

	private static final char[] CHARS = "abcdefghijklmnopqrstuvwxyz0123456789".toCharArray();
	private static final int NUMBER_OF_CHARS = CHARS.length;

	private static Random RANDOM = new Random();

	private static int generateRandomInt()
	{
		return RANDOM.nextInt( NUMBER_OF_CHARS );
	}

	private static char generateRandomChar()
	{
		return CHARS[generateRandomInt()];
	}

	public static String generateKey()
	{
		int keyLength = Realm.KEY_LENGTH;
		char[] key = new char[keyLength];
		for ( int i = 0; i < keyLength; i++ )
		{
			key[i] = generateRandomChar();
		}
		return new String( key );
	}
}
