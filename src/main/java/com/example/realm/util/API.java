package com.example.realm.util;

public final class API
{
	public API()
	{
	}

	public static final String API = "/api";
	public static final String REALM = API + "/realm";
}
