Spring Boot jar configuration exists by default with an embedded Tomcat. Changing package type from jar to war on pom.xml would suffice to deploy the app.

MySQL DB is used for application. 
Schema and credentials should be specified for related environment. 
Setting hibernate.ddl-auto" parameter on "application.properties" file to a suitable value 
eliminates the need for DB scripts.

Test codes are written.
H2 in-memory-DB is used only for testing.